### Réunion avec Miguel M. Sandin pour discuter des données 

Discussion avec le référant sur l'acquisition des données et la prépartation du travail.

### Bibliographie 

Etude des articles :

- Intra-genomic rDNA gene variability of Nassellaria and Spumellaria (Rhizaria, Radiolaria) assessed by Sanger, MinION and Illumina sequencing. (Sandin et al., 2021 (unpublished))

- Introducing ribosomal tandem repeat barcoding for fungi. (Wurzbacher et al., 2019) 

- Real-time DNA barcoding in a rainforest using nanopore sequencing: Opportunities for rapid biodiversity assessments and local
capacity building. (Pomerantz et al., 2018)


### Travail à partir du bash

Connexion au cluster de calcul de roscoff et familiarisation au pipline de bio-informatique à l'aide des documents  présents sur le GITHUB de notre référant. Exploration des différentes fonctions.


### Réunion n°2 avec Miguel M. Sandin

Questions sur les références proposées.
Questions sur le pipline de bio-informatique à mettre en place.

A ls suite de cette disucssion nous avons pu mettre en place des objectifs de résultats et de figures : 

A partir des données MinION on vient tester différents piplines bioinformatiques

1- séquence -> demultiplaxing -> Clustering -> Assignation NCBI -> Arbre phylogénétique (figure)

2- séquence -> Sans demultiplaxing -> clustering -> Assignation NCBI -> arbre phylogénétique (figure)

3- séquence -> demultiplexing - clustering -> définition de séquence consensus (consension) -> Assignation -> Arbre consensus

4- séquence -> demultiplexing - clustering -> Polishing -> Assigantion -> arbre phylo.

Si on a le temps, construction d'un arbre à partir des séquences obtenues par la méthode de Sanger et comparaison aux arbres obtenues à partir des données MinION.


### Extraction des données brutes à partir de la publication Sandin (2021) 

A partir du Github de M.M. Sandin, téléchargement et première analyse des données brutes. 
Les données brutes correspondent aux séquences directement issues des plateformes de séquençages : 
On a donc des compilation de "reads", soit des fragments compilés dans un seul fichier fasta par méthode de séquençage. 
Ici, les auteurs utilisent 3 méthodes différentes de séquençage : Illumina, Sanger et MinION. 
L'objectif est donc d'étudier les phylogénies de 4 groupes d'holobiontes de Radiolaires, issus des échantillonnages de la campagne MOOSE-GE en 2017. Ces holobiontes sélectionnés appartiennent aux familles des Rhizosphaeroidae, Spongosphaeroidae et des Nassellaria. 
Après leur isolation, les génomes ont été obtenus par SAG, selon les 3 méthodes énoncées précédemment. 
L'objectif est alors de construire des arbres phylogénétiques à partir des séquences obtenues avec la méthode MinION selon différents pipelines de traitements des séquences brutes. 

Nous avons alors commencé l'analyse des données brutes, à travers la construction d'arbres phylogénétiques dont les séquences n'ont subi aucune transformation. Les séquences ont d'abord été alignées, puis regroupées en clusters sur R (infos eliot)

Nous avons ensuite réalisé un schéma d'explication de la production des données à partir de l'article de Sandin et al., de l'échantillonnage jusqu'au séquençage. 


### Travail d'Assignation

Recherche pour trouver une base de données appropriée : choix de la base de données PR2 plutôt que la base de données NCBI car plus facile à utiliser localement.

[Guideline pour l'assignation avec DECIPHER et PR2](https://pr2database.github.io/pr2database/articles/pr2_04_decipher.html?fbclid=IwAR3_fsHC3dhuftIPTfetpLsiobUFz7-OXJNcJsfPjbYfAB7Wa4Vuee2GJlI)

Téléchargement de la base de données PR2 : 
```r
install.packages("pr2database")
library(pr2database)
data("pr2")
```

Construction d'un arbre et apprentissage de la taxonomie avec DECIPHER (environ 40mn):
```r
library(DECIPHER)
pr2$taxonomy = paste("Root", pr2$kingdom, pr2$supergroup, pr2$division, pr2$class, pr2$order, pr2$family, pr2$genus, pr2$species, sep = ";")

start = Sys.time()
training_dataset_18S = LearnTaxa(train = DNAStringSet(pr2$sequence),
          taxonomy = pr2$taxonomy,
          maxIterations = 3)
end = Sys.time()
difftime(end, start)
```

Assignation taxonomique (environ 15mn) :
```r
start = Sys.time()
minion_taxo = IdTaxa(test = minion_sequences,
        trainingSet = training_dataset_18S,
        processors = NULL)
end = Sys.time()
difftime(end, start)
```


### Traitement des données

Lecture du fichier Fastq et extraction des informations :
```r
minion_raw = readFastq("minion_raw.fastq")
minion = minion_raw[,2]
minion$Name = word(minion_raw$Header, 1, sep = fixed(" "))
minion$Run = word(word(minion_raw$Header, 2, sep = fixed("runid=")), 1, sep = fixed(" "))
minion$Channel = word(word(minion_raw$Header, 2, sep = fixed("ch=")), 1, sep = fixed(" "))
minion$Day = word(word(minion_raw$Header, 2, sep = fixed("start_time=")), 1, sep = fixed("T"))
minion$Time = word(word(minion_raw$Header, 2, sep = fixed("T")), 1, sep = fixed("Z"))
minion$Length = nchar(minion_raw$Sequence)
minion = tibble(cbind(minion[,-1], minion[,1]))
minion = minion[order(minion$Name),]
minion
```

Calcul du Qscore propre à Nanopore (Delahaye et al., 2020) après recherches bibliographiques (différent d'Illumina):
```r
minion$Qscore = apply(minion_raw[,3], 1, function(x) # Calculation of Nanopore Qscore (Delahaye et al., 2020)
  mean(0.015*(as.numeric(charToRaw(x)))^2 - 1.15*as.numeric(charToRaw(x)) + 24))
summary(minion$Qscore)
```

