# Notebook - Project MinION

Source article: [Sandin, M. M., Romac, S., & Not, F. (2021). Intra-genomic rDNA gene variability of Nassellaria and Spumellaria (Rhizaria, Radiolaria) assessed by Sanger, MinION and Illumina sequencing. bioRxiv.](https://www.biorxiv.org/content/10.1101/2021.10.05.463214v4.full.pdf)

Source code: [IntraGenomic-variability](https://github.com/MiguelMSandin/IntraGenomic-variability)

Raw data: [FigShare](https://figshare.com/articles/dataset/Intra-genomic_rDNA_gene_variability_of_Nassellaria_and_Spumellaria_Rhizaria_Radiolaria_assessed_by_Sanger_MinION_and_Illumina_sequencing/16922764/1)



### Question: MinION efficiency for metabarcoding studies of holobionts against Sanger sequencing 



### DAY 1 - Tuesday 30/11/2021

[Day 1 - Notes](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Day%201%20(30/11/2021)/NoteBook_Day1.md)


### DAY 2 - Wednesday 01/12/2021

[Day 2 - Notes](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Day%202%20(01/12/2021)/Notebook_day2.md)


### DAY 3 - Thursday 02/12/2021

[Day 3 - Notes](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Day%203%20(02/12/2021)/NoteBook_day3.md)

### DAY 4 - Friday 03/12/2021

[Day 4 - Notes](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Day%204%20(03/12/2021)/NoteBook_Day4)

<br>

# Oral presentation slides

[Slides](https://docs.google.com/presentation/d/1zwa6n6gRIzevW_BzmaTpcrJ-J5atydBtBOW9IJePeyA/edit)

<br>

# Report 

clic [here](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Rapport-MinION_RUIZ_CLEMENT_GUILLERMIN.pdf)

# Main representations of results

![Reads number](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20figures/Reads_number.png)

![Nassellaria](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20phylogenies/RAxML_bipartitions.cat_sanger_consensus_otu1_BS.png)

![Spumellaria 1](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20phylogenies/RAxML_bipartitions.cat_sanger_consensus_otu2_BS.png)

![Spumellaria 2](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20phylogenies/RAxML_bipartitions.cat_sanger_consensus_otu3_BS.png)

![Chrysophyceae](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20phylogenies/RAxML_bipartitions.cat_sanger_consensus_otu4_BS.png)

![Bacillariophyceae](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20phylogenies/RAxML_bipartitions.cat_sanger_consensus_otu5_BS.png)

![Error rate](https://gitlab.com/eliot_ruiz/minion/-/raw/main/Final%20figures/Error_rate_MinION.png)

