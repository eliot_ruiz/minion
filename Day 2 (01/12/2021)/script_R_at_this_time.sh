##### Etablissement d'un nouveau code pour la filtration des séquences, l'assignation, l'alignement et le clustering

## Initialisation

library(pr2database)
library(microseq)
library(DECIPHER)
library(tidyverse)
library(extrafont)


## Default theme for the following ggplot graph

theme_ = function(base_family = "Segoe UI Semilight", ...){
  theme_bw(base_family = base_family, ...) +
    theme(
      panel.grid = element_blank(),
      plot.title = element_text(hjust=0.5, vjust=0.5, face='bold',size=18),
      axis.title.y = element_text(margin=unit(c(0,0.2,0,0), "cm")),
      axis.text.y = element_text(margin=unit(c(0.1,0.1,0.1,0.1), "cm")),
      axis.ticks.length = unit(0.05, "in"))
}



## Loading the reference database for 18S

data("pr2")


## Loading the fastq file from from both Guppy versions

minion_2018_raw = readFastq("minion_2018_raw.fastq")
minion_2018_raw

minion_2020_raw = readFastq("minion_2020_raw.fastq")
minion_2020_raw



## Preparing the table for 2018

minion_2018 = minion_2018_raw[,2]
minion_2018$Name = word(minion_2018_raw$Header, 1, sep = fixed(" "))
minion_2018$Run = word(word(minion_2018_raw$Header, 2, sep = fixed("runid=")), 1, sep = fixed(" "))
minion_2018$Channel = word(word(minion_2018_raw$Header, 2, sep = fixed("ch=")), 1, sep = fixed(" "))
minion_2018$Day = word(word(minion_2018_raw$Header, 2, sep = fixed("start_time=")), 1, sep = fixed("T"))
minion_2018$Time = word(word(minion_2018_raw$Header, 2, sep = fixed("T")), 1, sep = fixed("Z"))
minion_2018$Length = nchar(minion_2018_raw$Sequence)

minion_2018$Qscore = apply(minion_2018_raw[,3], 1, function(x) # Calculation of Nanopore Qscore (Delahaye et al., 2020)
  mean(0.015*(as.numeric(charToRaw(x)))^2 - 1.15*as.numeric(charToRaw(x)) + 24))
summary(minion_2018$Qscore)

minion_2018 = tibble(cbind(minion_2018[,-1], minion_2018[,1]))
minion_2018 = minion_2018[order(minion_2018$Name),]
minion_2018


## Preparing the table for 2020

minion_2020 = minion_2020_raw[,2]
minion_2020$Name = word(minion_2020_raw$Header, 1, sep = fixed(" "))
minion_2020$Run = word(word(minion_2020_raw$Header, 2, sep = fixed("runid=")), 1, sep = fixed(" "))
minion_2020$Channel = word(word(minion_2020_raw$Header, 2, sep = fixed("ch=")), 1, sep = fixed(" "))
minion_2020$Day = word(word(minion_2020_raw$Header, 2, sep = fixed("start_time=")), 1, sep = fixed("T"))
minion_2020$Time = word(word(minion_2020_raw$Header, 2, sep = fixed("T")), 1, sep = fixed("Z"))
minion_2020$Length = nchar(minion_2020_raw$Sequence)

minion_2020$Qscore = apply(minion_2020_raw[,3], 1, function(x) # Calculation of Nanopore Qscore (Delahaye et al., 2020)
  mean(0.015*(as.numeric(charToRaw(x)))^2 - 1.15*as.numeric(charToRaw(x)) + 24))
summary(minion_2020$Qscore)

minion_2020 = tibble(cbind(minion_2020[,-1], minion_2020[,1]))
minion_2020 = minion_2020[order(minion_2020$Name),]
minion_2020


## Preparing the DNA string in a Biostring format

minion_2018_sequences = DNAStringSet(minion_2018$Sequence)
names(minion_2018_sequences) = minion_2018$Name
minion_2018_sequences

minion_2020_sequences = DNAStringSet(minion_2020$Sequence)
names(minion_2020_sequences) = minion_2020$Name
minion_2020_sequences


## Filtering Eukariotic 18S sequences with specific primer for 2018

eukaryotes_primers = "AACCTGGTTGATCCTGCCAGT" # SA (Medlin et al., 1988)
percentage_mismatch_primer = 30
number_mismatch_primer = round(mean(percentage_mismatch_primer*nchar(eukaryotes_primers)/100))
number_mismatch_primer

minion_2018_primer_found = vcountPattern(eukaryotes_primers, minion_2018_sequences, 
                                     max.mismatch = number_mismatch_primer)
minion_2018_primer_found

minion_2018_sequences_filtered = minion_2018_sequences[which(minion_2018_primer_found == 1)]
minion_2018_sequences_filtered

summary(width(minion_2018_sequences_filtered))

minion_2018_fasta_filtered = c(rbind(paste0(">", as.character(names(minion_2018_sequences_filtered))), as.character(minion_2018_sequences_filtered)))
write.table(minion_2018_fasta_filtered, file = "minion_2018_filtered.fasta", row.names = F, col.names = F, quote = F)


## Filtering Eukariotic 18S sequences with specific primer for 2020

minion_2020_primer_found = vcountPattern(eukaryotes_primers, minion_2020_sequences, 
                                         max.mismatch = number_mismatch_primer)
minion_2020_primer_found

minion_2020_sequences_filtered = minion_2020_sequences[which(minion_2020_primer_found == 1)]
minion_2020_sequences_filtered

summary(width(minion_2020_sequences_filtered))

minion_2020_fasta_filtered = c(rbind(paste0(">", as.character(names(minion_2020_sequences_filtered))), as.character(minion_2020_sequences_filtered)))
write.table(minion_2020_fasta_filtered, file = "minion_2020_filtered.fasta", row.names = F, col.names = F, quote = F)


## Assignation

pr2_fasta = c(rbind(paste0(">", pr2$kingdom, ";", pr2$supergroup, ";", pr2$division, ";", pr2$class, 
                           ";", pr2$order, ";", pr2$family, ";", pr2$genus, ";", pr2$species), FRAGMENT = pr2$sequence))
pr2_taxa = c("kingdom", "supergroup", "division", "class", "order", "family", "genus", "species")
write.table(pr2_fasta, file = "PR2.fasta", row.names = F, col.names = F, quote = F)

system("vsearch.exe --usearch_global minion_2018_filtered.fasta --db PR2.fasta --id 0 --maxaccepts 1 --blast6out minion_2018_blasted_1.tsv")
minion_2018_blasted_1 = fread("minion_2018_blasted_1.tsv")
colnames(minion_2018_blasted_1) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
minion_2018_blasted_1 = separate(data = minion_2018_blasted_1, col = target, into = pr2_taxa, sep = "\\;")
sort(table(minion_2018_blasted_1$order), decreasing = T)
minion_2018_blasted_1

system("vsearch.exe --usearch_global minion_2020_filtered.fasta --db PR2.fasta --id 0 --maxaccepts 1 --blast6out minion_2020_blasted_1.tsv")
minion_2020_blasted_1 = fread("minion_2020_blasted_1.tsv")
colnames(minion_2020_blasted_1) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
minion_2020_blasted_1 = separate(data = minion_2020_blasted_1, col = target, into = pr2_taxa, sep = "\\;")
sort(table(minion_2020_blasted_1$order), decreasing = T)
minion_2020_blasted_1


## Plotting some preliminary results

minion_blast_guppy = tibble(rbind(cbind(minion_2018_blasted_1, tibble(guppy = 2018)), cbind(minion_2020_blasted_1, tibble(guppy = 2020))))
minion_blast_guppy = tibble(merge(minion_blast_guppy, select(minion_2020, c("Name", "Length", "Qscore")), by.x = "query", by.y = "Name"))
minion_blast_guppy$graph_name = word(word(word(minion_blast_guppy$order, 1, sep = fixed("_")), 1, sep = fixed("-")), 1, sep = fixed(":"))
minion_blast_guppy$graph_name = factor(minion_blast_guppy$graph_name, levels = names(sort(table(minion_blast_guppy$graph_name), decreasing = T)))
minion_blast_guppy

ggplot(minion_blast_guppy, aes(x = graph_name)) + 
  facet_wrap(~guppy, nrow = 2, labeller = as_labeller(c("2018" = paste0("GUPPY 2018 (total = ", nrow(minion_2018_blasted_1), ")"),
                                                        "2020" = paste0("GUPPY 2020 (total = ", nrow(minion_2020_blasted_1), ")")))) + 
  geom_histogram(stat = "count") +
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Number of reads")
ggsave("MinION reads number.png", dpi = 900)

ggplot(minion_blast_guppy, aes(x = graph_name, y = as.numeric(Qscore))) + 
  facet_wrap(~guppy, nrow = 2) +
  geom_bar(position = "dodge", stat = "identity") + 
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Mean Nanopore quality score")
ggsave("MinION Qscore.png", dpi = 900)

ggplot(minion_blast_guppy, aes(x = graph_name, y = Length)) + 
  facet_wrap(~guppy, nrow = 2) +
  geom_bar(position = "dodge", stat = "identity") + 
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Mean read length (bp)")
ggsave("MinION Length.png", dpi = 900)


## Alignment of sequences

minion_blast_guppy$graph_name

minion_2018_sequences_aligned = AlignSeqs(minion_2018_sequences_filtered, processors = NULL)
minion_2018_sequences_aligned
BrowseSeqs(minion_2018_sequences_aligned)

minion_2018_fasta_aligned = c(rbind(paste0(">", as.character(names(minion_2018_sequences_aligned))), as.character(minion_2018_sequences_aligned)))
write.table(minion_2018_fasta_aligned, file = "minion_2018_aligned.fasta", row.names = F, col.names = F, quote = F)

minion_2020_sequences_aligned = AlignSeqs(minion_2020_sequences_filtered, processors = NULL)
minion_2020_sequences_aligned 
BrowseSeqs(minion_2020_sequences_aligned)

minion_2020_fasta_aligned = c(rbind(paste0(">", as.character(names(minion_2020_sequences_aligned))), as.character(minion_2020_sequences_aligned)))
write.table(minion_2020_fasta_aligned, file = "minion_2020_aligned.fasta", row.names = F, col.names = F, quote = F)


## Reformatting the names of the sequences

minion_2018_sequences_aligned = readDNAStringSet("minion_2018_aligned.fasta")
names2order_2018 = tibble(merge(tibble(initial_name = names(minion_2018_sequences_aligned)), 
                                select(subset(minion_blast_guppy, guppy == 2018), c("query", "graph_name")), by.x = "initial_name", by.y = "query"))
names2order_2018_list = Filter(nrow, split(names2order_2018, names2order_2018$graph_name))
names2order_2018 = bind_rows(lapply(names2order_2018_list, function(x) tibble(cbind(x, tibble(graph_name_num = paste0(x$graph_name, "_", 1:nrow(x)))))))
names2order_2018

minion_2020_sequences_aligned = readDNAStringSet("minion_2020_aligned.fasta")
names2order_2020 = tibble(merge(tibble(initial_name = names(minion_2020_sequences_aligned)), 
                                select(subset(minion_blast_guppy, guppy == 2020), c("query", "graph_name")), by.x = "initial_name", by.y = "query"))
names2order_2020_list = Filter(nrow, split(names2order_2020, names2order_2020$graph_name))
names2order_2020 = bind_rows(lapply(names2order_2020_list, function(x) tibble(cbind(x, tibble(graph_name_num = paste0(x$graph_name, "_", 1:nrow(x)))))))
names2order_2020

minion_2018_sequences_aligned = minion_2018_sequences_aligned[sort(names(minion_2018_sequences_aligned))]
names(minion_2018_sequences_aligned) = names2order_2018[order(names2order_2018$initial_name),]$graph_name_num
minion_2018_sequences_aligned

minion_2020_sequences_aligned = minion_2020_sequences_aligned[sort(names(minion_2020_sequences_aligned))]
names(minion_2020_sequences_aligned) = names2order_2020[order(names2order_2020$initial_name),]$graph_name_num
minion_2020_sequences_aligned


## Clustering 

minion_2018_distance = DistanceMatrix(minion_2018_sequences_aligned, correction = "Jukes-Cantor", processors = NULL)
minion_2018_clusters = IdClusters(minion_2018_distance, cutoff = 0.9, method = "NJ", processors = NULL, showPlot = T, 
                             myXStringSet = minion_2018_sequences_aligned, type = "both")
minion_2018_clusters

minion_2020_distance = DistanceMatrix(minion_2020_sequences_aligned, correction = "Jukes-Cantor", processors = NULL)
minion_2020_clusters = IdClusters(minion_2020_distance, cutoff = 0.9, method = "NJ", processors = NULL, showPlot = T, 
                                  myXStringSet = minion_2020_sequences_aligned, type = "both")
minion_2020_clusters


## Realigning radiolarians sequences

minion_2018_radiolarians_aligned = AdjustAlignment(minion_2018_sequences_aligned[grep("Nassellaria|Spumellaria", names(minion_2018_sequences_aligned))])
minion_2018_radiolarians_aligned

minion_2020_radiolarians_aligned = AdjustAlignment(minion_2020_sequences_aligned[grep("Nassellaria|Spumellaria", names(minion_2020_sequences_aligned))])
minion_2020_radiolarians_aligned

minion_2018_fasta_radiolarians_aligned = c(rbind(paste0(">", as.character(names(minion_2018_radiolarians_aligned))), as.character(minion_2018_radiolarians_aligned)))
write.table(minion_2018_fasta_radiolarians_aligned, file = "minion_2018_radiolarians_aligned.fasta", row.names = F, col.names = F, quote = F)

minion_2020_fasta_radiolarians_aligned = c(rbind(paste0(">", as.character(names(minion_2020_radiolarians_aligned))), as.character(minion_2020_radiolarians_aligned)))
write.table(minion_2020_fasta_radiolarians_aligned, file = "minion_2020_radiolarians_aligned.fasta", row.names = F, col.names = F, quote = F)


## Checking at which levels they form two distinct clusters -> 1.1% similarity

minion_2018_radiolarians_distance = DistanceMatrix(minion_2018_radiolarians_aligned, correction = "Jukes-Cantor", processors = NULL)
minion_2018_radiolarians_clusters = IdClusters(minion_2018_radiolarians_distance, cutoff = 1.1, method = "NJ", processors = NULL, showPlot = T, 
                                  myXStringSet = minion_2018_radiolarians_aligned, type = "both")
minion_2018_radiolarians_clusters

minion_2020_distance = DistanceMatrix(minion_2020_radiolarians_aligned, correction = "Jukes-Cantor", processors = NULL)
minion_2020_clusters = IdClusters(minion_2020_distance, cutoff = 1.1, method = "NJ", processors = NULL, showPlot = T, 
                                  myXStringSet = minion_2020_radiolarians_aligned, type = "both")
minion_2020_clusters
```

Problème : Résultats incohérents de clustering avec MOTHUR (création d'autant d'OTU que de séquence quelque soit le seuil ou la méthode adoptés)


##### Figures

![Nombre de reads](https://gitlab.com/eliot_ruiz/minion/uploads/48df26b52041b3bf61f086b31d78e370/MinION_reads_number.png)

![Taille des reads](https://gitlab.com/eliot_ruiz/minion/uploads/939cd66de32082fdb218252e42049777/MinION_Length.png)

![Qscore par méthode](https://gitlab.com/eliot_ruiz/minion/uploads/0eaad8124376f387c7e4ef35427a3aac/MinION_Qscore.png)

![Clusters à 99.1% pour GUPPY 2018](https://gitlab.com/eliot_ruiz/minion/uploads/61d6020caf9b677f60449a36e594108d/MinION_clusters_2018.png)

![Clusters à 99.1% pour GUPPY 2020](https://gitlab.com/eliot_ruiz/minion/uploads/1a61d0a4b81e96aed996397aa70929e7/MinION_clusters_2020.png)
