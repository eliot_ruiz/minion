## Visite de la plateforme GENOMER

Résumé des différentes technique de séquençage Sanger, Illumina, MinION.

Discussion sur le projet avec les technicien de la plateforme : suggestion d'utilisé une nouvelle version de logiciel de basecalling (GUPPY2020) pour construire une nouvelle base de données. 

## Réunion avec Miguel

Discussion du pipline de Bioinforamtique : 

#### MinION Guppy2018
1. RAW data (fasta5) - transformation des données avec _GUPPY 2018_ par les bio-informaticiens (en 2019)
2. Raw data (fastq = séquences nucléotidiques) 
3. Filtering à l'aide des primers SA et D2R-C  ~ filtre 30% de mismatch avec Rstudio Packages _DECYPHER_ 
4. Calcul de Qscore
5. Alignement
6. Clustering (avec mothur)
[Choix de méthode de clustering](https://doi.org/10.1016/j.ecolind.2019.105775)
7. Consensus
8. Poolishing

- Best match séquences : Blast avec la base de données pr2 via Vsearch en récupérant les 5 séquences les plus proches
- identification des outgroups

[MOTHER](https://mothur.org/wiki/cluster/)  


Alignement et clustering des séquences (définir un threshold) + outgroups + Best match sequences -> arbre phylogénétique 1 

Consensus sequence + outgroups + best match sequences -> arbre phylogénétique 2

Poolish consensus sequence + outgroups + Best match sequences -> arbres phylogénétique 3

_Nb : Arbre phylogénétique fait avec RaXML_


#### MinION Guppy2020

1.	Téléchargement des Raw data sur le GitHUB de M. Sandin au [format fast5](https://figshare.com/articles/dataset/Intra-genomic_rDNA_gene_variability_of_Nassellaria_and_Spumellaria_Rhizaria_Radiolaria_assessed_by_Sanger_MinION_and_Illumina_sequencing/16922764/1) 

Ces données correspondent aux signaux électroniques bruts directement issus de la lecture de la séquence par le nanopore du MinION. Le taux d’erreur de la technique MinION est en grande partie dû à la traduction des signaux électroniques en bases nucléotidiques, réalisée avec un logiciel de « basecalling ». Nous voulons tenter de réduire ce taux d’erreur au maximum pour optimiser la méthode MinION et donc prouver sa robustesse. Le logiciel utilisé par les bio-informaticiens pour traduire les signaux électroniques en bases est « Guppy ». Les arbres précédents sont réalisés avec la version 2018 du logiciel Guppy. Ces logiciels sont soumis à des améliorations constantes, les taux d’erreur ont peut-être diminué avec la version actuelle de Guppy disponible sur le cluster (Version d’octobre 2020 - v4.4.0), que nous allons alors utiliser ici pour diminuer le taux d’erreur (Q score) de lecture des séquences d’ADN. 

2.	Importation du fichier fast5 sur le cluster grâce à Filezilla

Le fichier contenant les signaux électroniques (fast5) est importé sur le cluster grâce au logiciel Filezilla, un client FTP libre d’accès qui permet de transférer des fichiers en local sur un hébergeur. 

3.	Traduction du fast5 en séquence nucléotidique (=fastq)
Traduction réalisée sur le cluster avec Guppy, [code ici](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Day%203%20(01/12/2021)/script.sh).

4.	Puis même pipeline bioinformatique qu'avec le fastq issu de _Guppy_2018_ :
5.	Filtering à l'aide des primers SA et D2R-C ~ filtre 30% de mismatch avec Rstudio Packages DECYPHER 
6.	Calcul de Qscore
7.	Alignement
8.	Clustering (avec mothur)
9.	Consensus
10.	Poolishing 


##### Changement de la méthode d'assignation

Alors que nous avions adopté le package DECIPHER pour les alignements, le clustering et l'assignation, car R est un language de code que nous maitrisons mieux que le bash, Miguel nous as informé que la méthode de clustering Neighbor-Joining adoptée sur R était très biaisée en bioinformatique. De plus, l'assignation avec la fonction "IdClusters" s'est avéré ne pas fonctionner, car tous les taxons avaient été assignés au nom "Root label" pour une raison incomprise. Nous avons donc choisi d'utiliser le programme bash vsearch qui a été conçu spécifiquement pour faire de l'assignation à partir d'une base de données locale, et nous a permis d'obtenir des résultats beaucoup plus cohérents.


##### Défintion des outgroupes

Candidats pour le groupes des Diatomées :
- [Bolidomonas](https://comptes-rendus.academie-sciences.fr/biologies/item/10.5802/crbiol.2.pdf) due to genetic proximity with diatoms
- [Coccoid haptophyte and Emiliania huxleyi, Bolidomonas](https://doi.org/10.1126/science.1096806)
- [Phaeodactylum tricornutum](https://doi.org/10.1016/j.bse.2013.03.025) -> diatomés
 


Candidats pour Chrysophyceae
- [Synchroma / Nannochloropsis](https://doi.org/10.1111/j.1756-1051.2013.00119.x) respectivement micro-algues amoeboid / microalgues 
- [Sellaphora blackfordensis and Nannochloropsis granulata](https://peerj.com/articles/2832/)
- [The sequences Synchroma grande
DQ788730, Leukarachnion sp. FJ356265 were chosen as 
outgroup according to Bock et al. (2014)](https://fottea.czechphycology.cz/pdfs/fot/2017/02/06.pdf)
- [Monodopsis subterranea, Pseudocharaciopsis minuta and  Eustigmatos magna](https://doi.org/10.1016/S1434-4610(99)70010-6)



Candidats pour Sphaeroplaeles

- [Fusochloris perforata and Chlorella ellipsoidea](https://doi.org/10.1046/j.1529-8817.2001.00162.x)
-  [Chaetophorales and Chaetopeltidales](https://doi.org/10.1016/j.ympev.2016.01.022)



Candidats pour Stemoniales 
- [Echinostelium arboreum, Barbeyella minitussima, Semimorula liquescens, Echinostelium coelocéphalum](https://doi.org/10.11646/phytotaxa.399.3.5) 


