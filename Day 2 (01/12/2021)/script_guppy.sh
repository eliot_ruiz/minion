''code bash 

# Chargement de l’outil guppy :
$ module load guppy/4.4.0-gpu
$ module list
Currently Loaded Modulefiles:
 1) singularity   2) guppy/4.4.0-gpu 
$ mkdir fastq

# Renseigner le fichier de configuration à partir 
# du type de flowcell (FLO-MIN107) et du kit de 
# séquençage (SQK-LSK308) : 
$ guppy_basecaller --print_workflows
FLO-MIN107 SQK-LSK308        dna_r9.5_450bps         unknown

# Lancement de l’outil guppy : 
$ guppy_basecaller -i /home/fr2424/stage/stage02/0 -s /home/fr2424/stage/stage02/fastq -c dna_r9.5_450bps.cfg 

# Importation du nouveau fichier fastq transformé en local :
scp stage02@bioinfo.sb-roscoff.fr:/home/fr2424/stage/stage02/fastq/* .
