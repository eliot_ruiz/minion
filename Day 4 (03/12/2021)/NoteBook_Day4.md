### [Finalisation du pipeline bio-informatique](https://l.facebook.com/l.php?u=https%3A%2F%2Fgitlab.com%2Feliot_ruiz%2Fminion%2F-%2Fblob%2Fmain%2FFinal%2520scripts%2FRendered_script.md%3Ffbclid%3DIwAR3gZdAPIFWS5D96pzTb4HcpZLAHTd-ubzqB2zAVuXT4PFI8dyTUZgDxQLo&h=AT1kHXf96CUNHX7HKgIJn6eAcnFXO-ghsPja6ddl0yIed7_4pI-idsE2oStFDsx0o2VLE6FJj2iYQ742RhISoWy8IFTFs0c82FAbLLEQy2atSYEr9gT65N6rhX0TkCPab2qMbA)


### Arbres Phylogénétiques et autres figures

Une fois les arbres phylogénétqiues produits, un travail de remodelage de l'arbre, par le réarrangement des groupes entre eux, routage de l'arbre par les outgroups, coloration des différentes banches remarquables de l'arbre. Cette étape est réalisé à partir de [FigTree](http://tree.bio.ed.ac.uk/software/figtree/)
 
 3 arbres par cluster définit était possible, nous avons choisi de présenter uniquement les arbres pour le cluster des Bacillariophycées et des Chryptophycées. 

Autres [figures](https://gitlab.com/eliot_ruiz/minion/-/tree/main/Final%20figures) produites: 
 - Nombre de lectures par cluster pour Sanger/MinION2018/MinION202
 - taille moyenne des lectures pour Sanger/MinION2018/MinION202
 - Score de qualité (Qscore) pour MinION 2018 et 2020
 - Moyenne de similarité avec le meilleur match (base de données pr2) pour Sanger/MinION2018/MinION202
 - Distance des séquences consensus + consensus polies par rapport a la séquence référence Sanger pour MinION 2018 et MinION 2020

### Finalisation de la présentation.

Partie résultats et discussion. Attribution des parties présentées par chaque poersonne du groupe. Entrainement à l'oral.

### Rédaction du rapport

Discussion des idées - des figures sélectionnées







