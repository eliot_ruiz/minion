# Rendered scripts assembled all together

### 1 - Bash code to run GUPPY 2020 on the Roscoff cluster (Fast5 transferred on the cluster using FileZilla)
```sh
## Connecting to Roscoff cluster (using MobaXterm on Windows)

ssh –Y stage02@bioinfo.sb-roscoff.fr 
# Need to enter the password then


## Loading GUPPY 2020 (10/2020 update)

module load guppy/4.4.0-gpu


## Creating a new directory on the cluster

mkdir fastq


## Printing all possible MinION configurations to get the name of the file containing specific settings

guppy_basecaller --print_workflows
# The flowcell FLO-MIN107 and the sequencing kit SQK-LSK308 were used -> infos in the file dna_r9.5_450bps.cfg 


## Launching GUPPY 2020 
guppy_basecaller -i /home/fr2424/stage/stage02/0 -s /home/fr2424/stage/stage02/fastq -c dna_r9.5_450bps.cfg 


## Exporting new FastQ to local folder
scp stage02@bioinfo.sb-roscoff.fr:/home/fr2424/stage/stage02/fastq/* .
```

### 2 - R code to extract Eukaryotes DNA from the two MinION FastQ files and find the closest match on PR2

```r
## Initialisation

library(pr2database)
library(microseq)
library(DECIPHER)
library(tidyverse)
library(extrafont)


## Loading the reference database for 18S

data("pr2")


## Loading the fastq file from from both Guppy versions

minion_2018_raw = readFastq("minion_2018_raw.fastq")
minion_2018_raw

minion_2020_raw = readFastq("minion_2020_raw.fastq")
minion_2020_raw



## Preparing the table for 2018

minion_2018 = minion_2018_raw[,2]
minion_2018$Name = word(minion_2018_raw$Header, 1, sep = fixed(" "))
minion_2018$Run = word(word(minion_2018_raw$Header, 2, sep = fixed("runid=")), 1, sep = fixed(" "))
minion_2018$Channel = word(word(minion_2018_raw$Header, 2, sep = fixed("ch=")), 1, sep = fixed(" "))
minion_2018$Day = word(word(minion_2018_raw$Header, 2, sep = fixed("start_time=")), 1, sep = fixed("T"))
minion_2018$Time = word(word(minion_2018_raw$Header, 2, sep = fixed("T")), 1, sep = fixed("Z"))
minion_2018$Length = nchar(minion_2018_raw$Sequence)

minion_2018$Qscore = apply(minion_2018_raw[,3], 1, function(x) # Calculation of Nanopore Qscore (Delahaye et al., 2020)
  mean(0.015*(as.numeric(charToRaw(x)))^2 - 1.15*as.numeric(charToRaw(x)) + 24))
summary(minion_2018$Qscore)

minion_2018 = tibble(cbind(minion_2018[,-1], minion_2018[,1]))
minion_2018 = minion_2018[order(minion_2018$Name),]
minion_2018


## Preparing the table for 2020

minion_2020 = minion_2020_raw[,2]
minion_2020$Name = word(minion_2020_raw$Header, 1, sep = fixed(" "))
minion_2020$Run = word(word(minion_2020_raw$Header, 2, sep = fixed("runid=")), 1, sep = fixed(" "))
minion_2020$Channel = word(word(minion_2020_raw$Header, 2, sep = fixed("ch=")), 1, sep = fixed(" "))
minion_2020$Day = word(word(minion_2020_raw$Header, 2, sep = fixed("start_time=")), 1, sep = fixed("T"))
minion_2020$Time = word(word(minion_2020_raw$Header, 2, sep = fixed("T")), 1, sep = fixed("Z"))
minion_2020$Length = nchar(minion_2020_raw$Sequence)

minion_2020$Qscore = apply(minion_2020_raw[,3], 1, function(x) # Calculation of Nanopore Qscore (Delahaye et al., 2020)
  mean(0.015*(as.numeric(charToRaw(x)))^2 - 1.15*as.numeric(charToRaw(x)) + 24))
summary(minion_2020$Qscore)

minion_2020 = tibble(cbind(minion_2020[,-1], minion_2020[,1]))
minion_2020 = minion_2020[order(minion_2020$Name),]
minion_2020


## Preparing the DNA string in a Biostring format

minion_2018_sequences = DNAStringSet(minion_2018$Sequence)
names(minion_2018_sequences) = minion_2018$Name
minion_2018_sequences

minion_2020_sequences = DNAStringSet(minion_2020$Sequence)
names(minion_2020_sequences) = minion_2020$Name
minion_2020_sequences

minion_2018_sequences = c(rbind(paste0(">", as.character(names(minion_2018_sequences))), as.character(minion_2018_sequences)))
write.table(minion_2018_sequences, file = "minion_2018_raw.fasta", row.names = F, col.names = F, quote = F)

minion_2020_sequences = c(rbind(paste0(">", as.character(names(minion_2020_sequences))), as.character(minion_2020_sequences)))
write.table(minion_2020_sequences, file = "minion_2020_raw.fasta", row.names = F, col.names = F, quote = F)


## Filtering Eukariotic ribosomal sequences with specific primer and length (SA = Medlin et al., 1988 // R_RC = search ref)

system('cutadapt-3.4.exe -g AACCTGGTTGATCCTGCCAGT -a TCTTGAAACACGGACCAAGG --discard-untrimmed --minimum-length 2000 -o "minion_2018_filtered.fasta" "minion_2018_raw.fasta"')
minion_2018_fasta_filtered = readDNAStringSet("minion_2018_filtered.fasta")
minion_2018_fasta_filtered

system('cutadapt-3.4.exe -g AACCTGGTTGATCCTGCCAGT -a TCTTGAAACACGGACCAAGG --discard-untrimmed --minimum-length 2000 -o "minion_2020_filtered.fasta" "minion_2020_raw.fasta"')
minion_2020_fasta_filtered = readDNAStringSet("minion_2020_filtered.fasta")
minion_2020_fasta_filtered


## Assignation

pr2_fasta = c(rbind(paste0(">", pr2$kingdom, ";", pr2$supergroup, ";", pr2$division, ";", pr2$class, 
                           ";", pr2$order, ";", pr2$family, ";", pr2$genus, ";", pr2$species), FRAGMENT = pr2$sequence))
pr2_taxa = c("kingdom", "supergroup", "division", "class", "order", "family", "genus", "species")
write.table(pr2_fasta, file = "PR2.fasta", row.names = F, col.names = F, quote = F)

system("vsearch.exe --usearch_global minion_2018_filtered.fasta --db PR2.fasta --id 0 --maxaccepts 1 --blast6out minion_2018_blasted_1.tsv")
minion_2018_blasted_1 = fread("minion_2018_blasted_1.tsv")
colnames(minion_2018_blasted_1) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
minion_2018_blasted_1 = separate(data = minion_2018_blasted_1, col = target, into = pr2_taxa, sep = "\\;")
sort(table(minion_2018_blasted_1$order), decreasing = T)
minion_2018_blasted_1

system("vsearch.exe --usearch_global minion_2020_filtered.fasta --db PR2.fasta --id 0 --maxaccepts 1 --blast6out minion_2020_blasted_1.tsv")
minion_2020_blasted_1 = fread("minion_2020_blasted_1.tsv")
colnames(minion_2020_blasted_1) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
minion_2020_blasted_1 = separate(data = minion_2020_blasted_1, col = target, into = pr2_taxa, sep = "\\;")
sort(table(minion_2020_blasted_1$order), decreasing = T)
minion_2020_blasted_1


## Changing the name of the first assignation

minion_blast_guppy = tibble(rbind(cbind(minion_2018_blasted_1, tibble(guppy = 2018)), cbind(minion_2020_blasted_1, tibble(guppy = 2020))))
minion_blast_guppy = tibble(merge(minion_blast_guppy, select(minion_2020, c("Name", "Length", "Qscore")), by.x = "query", by.y = "Name"))
minion_blast_guppy$graph_name = word(word(word(minion_blast_guppy$order, 1, sep = fixed("_")), 1, sep = fixed("-")), 1, sep = fixed(":"))
minion_blast_guppy$graph_name = factor(minion_blast_guppy$graph_name, levels = names(sort(table(minion_blast_guppy$graph_name), decreasing = T)))
minion_blast_guppy


## Reformatting the names of the sequences for treatment in Linux

minion_2018_sequences_filtered = readDNAStringSet("minion_2018_filtered.fasta")
names2order_2018 = tibble(merge(tibble(initial_name = names(minion_2018_sequences_filtered)), 
                                select(subset(minion_blast_guppy, guppy == 2018), c("query", "graph_name")), by.x = "initial_name", by.y = "query"))
names2order_2018_list = Filter(nrow, split(names2order_2018, names2order_2018$graph_name))
names2order_2018 = bind_rows(lapply(names2order_2018_list, function(x) tibble(cbind(x, tibble(graph_name_num = paste0(x$graph_name, "_", 1:nrow(x)))))))
names2order_2018

minion_2020_sequences_filtered = readDNAStringSet("minion_2020_filtered.fasta")
names2order_2020 = tibble(merge(tibble(initial_name = names(minion_2020_sequences_filtered)), 
                                select(subset(minion_blast_guppy, guppy == 2020), c("query", "graph_name")), by.x = "initial_name", by.y = "query"))
names2order_2020_list = Filter(nrow, split(names2order_2020, names2order_2020$graph_name))
names2order_2020 = bind_rows(lapply(names2order_2020_list, function(x) tibble(cbind(x, tibble(graph_name_num = paste0(x$graph_name, "_", 1:nrow(x)))))))
names2order_2020

minion_2018_sequences_filtered = minion_2018_sequences_filtered[sort(names(minion_2018_sequences_filtered))]
names(minion_2018_sequences_filtered) = names2order_2018[order(names2order_2018$initial_name),]$graph_name_num
minion_2018_sequences_filtered

minion_2020_sequences_filtered = minion_2020_sequences_filtered[sort(names(minion_2020_sequences_filtered))]
names(minion_2020_sequences_filtered) = names2order_2020[order(names2order_2020$initial_name),]$graph_name_num
minion_2020_sequences_filtered

minion_2018_fasta_filtered = c(rbind(paste0(">", as.character(names(minion_2018_sequences_filtered))), as.character(minion_2018_sequences_filtered)))
write.table(minion_2018_fasta_filtered, file = "minion_2018_filtered_assigned.fasta", row.names = F, col.names = F, quote = F)

minion_2020_fasta_filtered = c(rbind(paste0(">", as.character(names(minion_2020_sequences_filtered))), as.character(minion_2020_sequences_filtered)))
write.table(minion_2020_fasta_filtered, file = "minion_2020_filtered_assigned.fasta", row.names = F, col.names = F, quote = F)
```

### 3 - Bash code to extract Eukaryotes DNA from the two MinION FastQ files and find the closest match on PR2

```sh
## Installing required packages

sudo apt-get install mafft
sudo apt-get install mothur
sudo apt-get install minimap2
sudo apt-get install racon


## MAFFT: Aligning assigned and filtered sequences 

mafft "minion_2018_filtered_assigned.fasta" > "minion_2018_aligned.fasta"
mafft "minion_2020_filtered_assigned.fasta" > "minion_2020_aligned.fasta"


## MOTHUR: Clustering aligned sequences (distance matrix first before clustering based on distances)

ALIGNMENT_2018="minion_2018_aligned.fasta"
ALIGNMENT_2020="minion_2020_aligned.fasta"

mothur "#dist.seqs(fasta=$ALIGNMENT_2018, output=lt, processors=2)" 
mothur "#dist.seqs(fasta=$ALIGNMENT_2020, output=lt, processors=2)"

mothur "#cluster(phylip=${ALIGNMENT_2018/.fasta/.phylip.dist}, cutoff=0.35)"
mothur "#cluster(phylip=${ALIGNMENT_2020/.fasta/.phylip.dist}, cutoff=0.35)"
# The cutoff of 0.35 (35% of similarity per cluster) was selected subjectively based on the correspondence with classes


## CONSENSION: Creating a consensus sequences per cluster

chmod +x consension 
./consension -i $ALIGNMENT_2018 -t ${ALIGNMENT_2018/.fasta/.phylip.opti_mcc.list} -o "minion_2018_consensus_raw.fasta" -K 3 -R T -T T
./consension -i $ALIGNMENT_2020 -t ${ALIGNMENT_2020/.fasta/.phylip.opti_mcc.list} -o "minion_2020_consensus_raw.fasta" -K 3 -R T -T T

sed -i 's/\.//g' "minion_2018_consensus_raw.fasta"
sed -i 's/\.//g' "minion_2020_consensus_raw.fasta"


## MINIMAP2: Mapping FastQ quality informations to each nucleotide in the consensus sequences

minimap2 "minion_2018_consensus_raw.fasta" "minion_2018_raw.fastq" > "minimap_racon_2018.paf"
minimap2 "minion_2020_consensus_raw.fasta" "minion_2020_raw.fastq" > "minimap_racon_2020.paf"


## RACON: Polishing consensus sequences using the FastQ quality informations

racon "minion_2018_raw.fastq" "minimap_racon_2018.paf" "minion_2018_consensus_raw.fasta" > "minion_2018_consensus_poolished.fasta"
racon "minion_2020_raw.fastq" "minimap_racon_2020.paf" "minion_2020_consensus_raw.fasta" > "minion_2020_consensus_poolished.fasta"
```

### 4 - R code to choose MinION OTU, prepare the reference tree (reference sequences + outgroups) and prepare the names of sequences

```r
## Choosing the most interesting OTU after treatment in Linux

mothur2table = function(file){
  
  table = fread(file)
  
  for(i in 3:length(table)){
    
    otu_table = tibble(cbind(separate_rows(tibble(name = table[[i]]), name, sep = ","), tibble(otu = names(table)[i])))
    
    otu_table$name[which(otu_table$name == "NA")] = NA
    
    if(i == 3) all_otu_table = otu_table
    
    else all_otu_table = rbind(all_otu_table, otu_table)
    
  }
  
  return(all_otu_table)
  
}

minion_2018_otu = mothur2table("first_linux_result/minion_2018_aligned.phylip.opti_mcc.list")
minion_2018_otu

minion_2020_otu = mothur2table("first_linux_result/minion_2020_aligned.phylip.opti_mcc.list")
minion_2020_otu

table(word(minion_2018_otu$name, 1, sep = fixed("_")), minion_2018_otu$otu)
table(word(minion_2020_otu$name, 1, sep = fixed("_")), minion_2020_otu$otu)

minion_2018_chosen_otu = subset(minion_2018_otu, otu %in% c("Otu01", "Otu02", "Otu03", "Otu05", "Otu06"))
minion_2020_chosen_otu = subset(minion_2020_otu, otu %in% c("Otu01", "Otu02", "Otu03", "Otu05", "Otu06"))


## Checking if the two Spumellaria clusters correspond to the two species

unique(subset(minion_2018_blasted_1, query %in% subset(names2order_2018, graph_name_num %in% 
                                                         subset(minion_2018_otu, otu == "Otu02")$name)$initial_name)$species) # OTU2 = Rhizosphaera trigonacantha

unique(subset(minion_2018_blasted_1, query %in% subset(names2order_2018, graph_name_num %in% 
                                                         subset(minion_2018_otu, otu == "Otu03")$name)$initial_name)$species) # OTU3 = Spongosphaera streptacantha

unique(subset(minion_2020_blasted_1, query %in% subset(names2order_2020, graph_name_num %in% 
                                                         subset(minion_2020_otu, otu == "Otu02")$name)$initial_name)$species) # OTU2 = Spongosphaera streptacantha

unique(subset(minion_2020_blasted_1, query %in% subset(names2order_2020, graph_name_num %in% 
                                                         subset(minion_2020_otu, otu == "Otu03")$name)$initial_name)$species) # OTU3 = Rhizosphaera trigonacantha

############ REALIZED THIS PROBLEM LATTER: NEED TO CHANGE MANUALLY FILES NAME FOR 2018 ###########


## Adding the species and separating by OTU

minion_2018_seq_table = tibble(name = names(minion_2020_sequences_filtered), sequence = paste(minion_2020_sequences_filtered))
minion_2018_seq_table

minion_2020_seq_table = tibble(name = names(minion_2020_sequences_filtered), sequence = paste(minion_2020_sequences_filtered))
minion_2020_seq_table

minion_2018_chosen_otu_seq = tibble(merge(minion_2018_chosen_otu, minion_2018_seq_table))
minion_2018_chosen_otu_seq = tibble(merge(names2order_2018[,-2], minion_2018_chosen_otu_seq, by.x = "graph_name_num", by.y = "name"))
minion_2018_chosen_otu_seq

minion_2020_chosen_otu_seq = tibble(merge(minion_2020_chosen_otu, minion_2020_seq_table))
minion_2020_chosen_otu_seq = tibble(merge(names2order_2020[,-2], minion_2020_chosen_otu_seq, by.x = "graph_name_num", by.y = "name"))
minion_2020_chosen_otu_seq

minion_2018_chosen_otu_seq = tibble(merge(select(minion_2018_blasted_1, c("query", "species")), minion_2018_chosen_otu_seq, 
                                          by.x = "query", by.y = "initial_name"))
minion_2018_chosen_otu_seq

minion_2020_chosen_otu_seq = tibble(merge(select(minion_2020_blasted_1, c("query", "species")), minion_2020_chosen_otu_seq, 
                                          by.x = "query", by.y = "initial_name"))
minion_2020_chosen_otu_seq

minion_2018_chosen_otu_dna = lapply(split(minion_2018_chosen_otu_seq, minion_2018_chosen_otu_seq$otu), function(x) 
  setNames(DNAStringSet(x$sequence), x$species))
minion_2018_chosen_otu_dna 

minion_2020_chosen_otu_dna = lapply(split(minion_2020_chosen_otu_seq, minion_2020_chosen_otu_seq$otu), function(x) 
  setNames(DNAStringSet(x$sequence), x$species))
minion_2020_chosen_otu_dna 


## Choosing 3 representative raw sequences with as much different assignation as possible

minion_2018_chosen_otu_dna = lapply(minion_2018_chosen_otu_dna, function(x) x[order(names(x)),])
minion_2018_chosen_otu_dna = lapply(minion_2018_chosen_otu_dna, function(x) x[c(1, floor(length(x)/2), length(x)),])
minion_2018_chosen_otu_dna = lapply(minion_2018_chosen_otu_dna, function(x) setNames(x, paste0("RRR_", names(x))))
minion_2018_chosen_otu_dna

minion_2020_chosen_otu_dna = lapply(minion_2020_chosen_otu_dna, function(x) x[order(names(x)),])
minion_2020_chosen_otu_dna = lapply(minion_2020_chosen_otu_dna, function(x) x[c(1, floor(length(x)/2), length(x)),])
minion_2020_chosen_otu_dna = lapply(minion_2020_chosen_otu_dna, function(x) setNames(x, paste0("RRR_", names(x))))
minion_2020_chosen_otu_dna

for(i in 1:length(minion_2018_chosen_otu_dna)){
  
  minion_2018_chosen_otu_fasta = c(rbind(paste0(">", as.character(names(minion_2018_chosen_otu_dna[[i]]))), paste(minion_2018_chosen_otu_dna[[i]])))
  write.table(minion_2018_chosen_otu_fasta, file = paste0("final_seqs/minion_2018_random_seq_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}


for(i in 1:length(minion_2020_chosen_otu_dna)){
  
  minion_2020_chosen_otu_fasta = c(rbind(paste0(">", as.character(names(minion_2020_chosen_otu_dna[[i]]))), paste(minion_2020_chosen_otu_dna[[i]])))
  write.table(minion_2020_chosen_otu_fasta, file = paste0("final_seqs/minion_2020_random_seq_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}


## Filtering the only the five selected OTU in the concensus and adding the tag

minion_2018_consensus_raw = readDNAStringSet("first_linux_result/minion_2018_consensus_raw.fasta")
minion_2018_consensus_raw = minion_2018_consensus_raw[which(names(minion_2018_consensus_raw) %in% c("Otu01", "Otu02", "Otu03", "Otu05", "Otu06"))]
names(minion_2018_consensus_raw) = rep("Only_consensus_2018", 5)
minion_2018_consensus_raw

minion_2020_consensus_raw = readDNAStringSet("first_linux_result/minion_2020_consensus_raw.fasta")
minion_2020_consensus_raw = minion_2020_consensus_raw[which(names(minion_2020_consensus_raw) %in% c("Otu01", "Otu02", "Otu03", "Otu05", "Otu06"))]
names(minion_2020_consensus_raw) = rep("Only_consensus_2020", 5)
minion_2020_consensus_raw

for(i in 1:length(minion_2018_consensus_raw)){
  
  minion_2018_consensus_fasta = c(rbind(paste0(">", as.character(names(minion_2018_consensus_raw)[i])), paste(minion_2018_consensus_raw[i])))
  write.table(minion_2018_consensus_fasta, file = paste0("final_seqs/minion_2018_consensus_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}

for(i in 1:length(minion_2020_consensus_raw)){
  
  minion_2020_consensus_fasta = c(rbind(paste0(">", as.character(names(minion_2020_consensus_raw)[i])), paste(minion_2020_consensus_raw[i])))
  write.table(minion_2020_consensus_fasta, file = paste0("final_seqs/minion_2020_consensus_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}


## Filtering the five selected OTU in the polished consensus and adding the tag

minion_2018_polished_consensus = readDNAStringSet("first_linux_result/minion_2018_consensus_poolished.fasta")
names(minion_2018_polished_consensus) = word(names(minion_2018_polished_consensus), 1, sep = fixed(" "))
minion_2018_polished_consensus = minion_2018_polished_consensus[which(names(minion_2018_polished_consensus) %in% c("Otu01", "Otu02", "Otu03", "Otu05", "Otu06"))]
names(minion_2018_polished_consensus) = rep("Polished_consensus_2018", 5)
minion_2018_polished_consensus

minion_2020_polished_consensus = readDNAStringSet("first_linux_result/minion_2020_consensus_poolished.fasta")
names(minion_2020_polished_consensus) = word(names(minion_2020_polished_consensus), 1, sep = fixed(" "))
minion_2020_polished_consensus = minion_2020_polished_consensus[which(names(minion_2020_polished_consensus) %in% c("Otu01", "Otu02", "Otu03", "Otu05", "Otu06"))]
names(minion_2020_polished_consensus) = rep("Polished_consensus_2020", 5)
minion_2020_polished_consensus

for(i in 1:length(minion_2018_polished_consensus)){
  
  minion_2018_polished_fasta = c(rbind(paste0(">", as.character(names(minion_2018_polished_consensus)[i])), paste(minion_2018_polished_consensus[i])))
  write.table(minion_2018_polished_fasta, file = paste0("final_seqs/minion_2018_polished_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}

for(i in 1:length(minion_2020_polished_consensus)){
  
  minion_2020_polished_fasta = c(rbind(paste0(">", as.character(names(minion_2020_polished_consensus)[i])), paste(minion_2020_polished_consensus[i])))
  write.table(minion_2020_polished_fasta, file = paste0("final_seqs/minion_2020_polished_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}


## Searching the 30 closest sequences to polished_consensus_sequence

pr2_species_fasta = c(rbind(paste0(">", pr2$pr2_accession, ";", pr2$species), FRAGMENT = pr2$sequence))
pr2_species_taxa = c("accession", "species")
write.table(pr2_species_fasta, file = "PR2_species.fasta", row.names = F, col.names = F, quote = F)

for(i in 1:length(minion_2018_polished_consensus)){
  
  system(paste0("vsearch.exe --usearch_global final_seqs/minion_2018_polished_otu", i, 
                ".fasta --db PR2_species.fasta --id 0 --maxaccepts 30 --blast6out minion_2018_reference_otu", i, ".tsv"))
  
  minion_2018_blasted_reference = fread(paste0("final_seqs/minion_2018_reference_otu", i, ".tsv"))
  colnames(minion_2018_blasted_reference) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
  minion_2018_blasted_reference = separate(data = minion_2018_blasted_reference, col = target, into = pr2_species_taxa, sep = "\\;")
  minion_2018_blasted_reference = select(subset(pr2, pr2_accession %in% minion_2018_blasted_reference$accession), c("species", "sequence"))
  
  minion_2018_reference_fasta = c(rbind(paste0(">", as.character(minion_2018_blasted_reference$species[i])), as.character(minion_2018_blasted_reference$sequence[i])))
  write.table(minion_2018_reference_fasta, file = paste0("final_seqs/minion_2018_reference_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}

for(i in 1:length(minion_2020_polished_consensus)){
  
  system(paste0("vsearch.exe --usearch_global final_seqs/minion_2020_polished_otu", i, 
                ".fasta --db PR2_species.fasta --id 0 --maxaccepts 30 --blast6out minion_2020_reference_otu", i, ".tsv"))
  
  minion_2020_blasted_reference = fread(paste0("final_seqs/minion_2020_reference_otu", i, ".tsv"))
  colnames(minion_2020_blasted_reference) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
  minion_2020_blasted_reference = separate(data = minion_2020_blasted_reference, col = target, into = pr2_species_taxa, sep = "\\;")
  minion_2020_blasted_reference = select(subset(pr2, pr2_accession %in% minion_2020_blasted_reference$accession), c("species", "sequence"))
  
  minion_2020_reference_fasta = c(rbind(paste0(">", as.character(minion_2020_blasted_reference$species)), as.character(minion_2020_blasted_reference$sequence)))
  write.table(minion_2020_reference_fasta, file = paste0("final_seqs/minion_2020_reference_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}


## Treating the outgroups

# Radiolarians outgroups

radiolarians_id_outgroups = tibble(read.csv("ID_outgroups_nass-spum.csv", header = F))$V1
radiolarians_id_outgroups

radiolarians_outgroups = select(subset(pr2, genbank_accession %in% radiolarians_id_outgroups), c("class", "sequence"))
radiolarians_outgroups$class = replace(radiolarians_outgroups$class, which(radiolarians_outgroups$class == "Radiolaria_X"), "Rad-X")
unique(radiolarians_outgroups$class)

radiolarians_outgroups_fasta = c(rbind(paste0(">", as.character(radiolarians_outgroups$class)), as.character(radiolarians_outgroups$sequence)))
write.table(radiolarians_outgroups_fasta, file = "final_seqs/radiolarians_outgroups.fasta", row.names = F, col.names = F, quote = F)


# Diatoms outgroups

system("vsearch.exe --usearch_global diatoms_outgroups.fasta --db PR2_species.fasta --id 0 --maxaccepts 5 --blast6out diatoms_outgroups.tsv")

diatoms_outgroups = fread("final_seqs/diatoms_outgroups.tsv")
colnames(diatoms_outgroups) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
diatoms_outgroups = separate(data = diatoms_outgroups, col = target, into = pr2_species_taxa, sep = "\\;")
diatoms_outgroups = select(subset(pr2, pr2_accession %in% diatoms_outgroups$accession), c("class", "sequence"))
diatoms_outgroups
unique(diatoms_outgroups$class)

diatoms_outgroups_fasta = c(rbind(paste0(">", as.character(diatoms_outgroups$class)), as.character(diatoms_outgroups$sequence)))
write.table(diatoms_outgroups_fasta, file = "final_seqs/diatoms_outgroups.fasta", row.names = F, col.names = F, quote = F)


# Chrysophyceae outgroups

system("vsearch.exe --usearch_global chrysophyceae_outgroups.fasta --db PR2_species.fasta --id 0 --maxaccepts 5 --blast6out chrysophyceae_outgroups.tsv")

chrysophyceae_outgroups = fread("final_seqs/chrysophyceae_outgroups.tsv")
colnames(chrysophyceae_outgroups) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
chrysophyceae_outgroups = separate(data = chrysophyceae_outgroups, col = target, into = pr2_species_taxa, sep = "\\;")
chrysophyceae_outgroups = select(subset(pr2, pr2_accession %in% chrysophyceae_outgroups$accession), c("genus", "sequence"))
chrysophyceae_outgroups
unique(chrysophyceae_outgroups$genus)

chrysophyceae_outgroups_fasta = c(rbind(paste0(">", as.character(chrysophyceae_outgroups$genus)), as.character(chrysophyceae_outgroups$sequence)))
write.table(chrysophyceae_outgroups_fasta, file = "final_seqs/chrysophyceae_outgroups.fasta", row.names = F, col.names = F, quote = F)


## Compiling the outgroups and the reference in the same fasta file

radiolarians_outgroups_dna = readDNAStringSet("final_seqs/radiolarians_outgroups.fasta")
diatoms_outgroups_dna = readDNAStringSet("final_seqs/diatoms_outgroups.fasta")
chrysophyceae_outgroups_dna = readDNAStringSet("final_seqs/chrysophyceae_outgroups.fasta")

for(i in 1:length(minion_2020_polished_consensus)){

  reference_2020 = readDNAStringSet(paste0("final_seqs/minion_2020_reference_otu", i, ".fasta"))
  
  if(i %in% 1:3) outgroups_reference_2020 = c(radiolarians_outgroups_dna, reference_2020)
  
  else if(i == 4) outgroups_reference_2020 = c(chrysophyceae_outgroups_dna, reference_2020)
  
  else outgroups_reference_2020 = c(diatoms_outgroups_dna, reference_2020)
  
  outgroups_reference_2020_fasta = c(rbind(paste0(">", names(outgroups_reference_2020)), paste(outgroups_reference_2020)))
  write.table(outgroups_reference_2020_fasta, file = paste0("final_seqs/outgroups_reference_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)

}
```

### 5 - R code to extract Eukaryotes DNA from the Sanger concatenated file and find the closest match on PR2

```r
## Reading concatenated files (step made by Miguel Sandin on Python with fastaConcat.py)

sanger_sequences = readDNAStringSet("sanger_concatenated.fasta")
sanger_sequences


## Filtering Eukariotic ribosomal sequences with specific primer and length (SA = Medlin et al., 1988 // R_RC = search ref)

system('cutadapt-3.4.exe -g AACCTGGTTGATCCTGCCAGT -a TCTTGAAACACGGACCAAGG --discard-untrimmed --minimum-length 2000 -o "sanger_filtered.fasta" "sanger_concatenated.fasta"')
sanger_filtered = readDNAStringSet("sanger_filtered.fasta")
sanger_filtered

summary(width(sanger_filtered))


## Assignation

system("vsearch.exe --usearch_global sanger_filtered.fasta --db PR2.fasta --id 0 --maxaccepts 1 --blast6out sanger_blasted_1.tsv")
sanger_blasted_1 = fread("sanger_blasted_1.tsv")
colnames(sanger_blasted_1) = c("query", "target", "id", "alnlen", "mism", "opens", "qlo", "qhi", "tlo", "thi", "evalue", "bits")
sanger_blasted_1 = separate(data = sanger_blasted_1, col = target, into = pr2_taxa, sep = "\\;")
sanger_blasted_1$graph_name = word(word(word(sanger_blasted_1$order, 1, sep = fixed("_")), 1, sep = fixed("-")), 1, sep = fixed(":"))
sanger_blasted_1$graph_name = factor(sanger_blasted_1$graph_name, levels = names(sort(table(sanger_blasted_1$graph_name), decreasing = T)))
sanger_blasted_1


## Renaming of sequences

names2order_sanger = tibble(merge(tibble(initial_name = names(sanger_filtered)), 
                                  select(sanger_blasted_1, c("query", "graph_name")), by.x = "initial_name", by.y = "query"))
names2order_sanger_list = Filter(nrow, split(names2order_sanger, names2order_sanger$graph_name))
names2order_sanger = bind_rows(lapply(names2order_sanger_list, function(x) tibble(cbind(x, tibble(graph_name_num = paste0(x$graph_name, "_", 1:nrow(x)))))))
names2order_sanger

sanger_sequences_filtered = sanger_sequences_filtered[sort(names(sanger_sequences_filtered))]
names(sanger_sequences_filtered) = names2order_sanger[order(names2order_sanger$initial_name),]$graph_name_num
sanger_sequences_filtered

sanger_fasta_filtered = c(rbind(paste0(">", as.character(names(sanger_sequences_filtered))), as.character(sanger_sequences_filtered)))
write.table(sanger_fasta_filtered, file = "sanger_filtered_assigned.fasta", row.names = F, col.names = F, quote = F)
```

### 6 - Bash code to extract Eukaryotes DNA from the Sanger sequences and find the closest match on PR2

```sh
## Changing directory

cd ../sanger


## MAFFT: Aligning assigned and filtered sequences 

mafft "sanger_filtered_assigned.fasta" > "sanger_filtered_aligned.fasta"


## MOTHUR: Clustering aligned sequences (distance matrix first before clustering based on distances)

ALIGNMENT_SANGER="sanger_filtered_aligned.fasta"

mothur "#dist.seqs(fasta=$ALIGNMENT_SANGER, output=lt, processors=2)"

mothur "#cluster(phylip=${ALIGNMENT_SANGER/.fasta/.phylip.dist}, cutoff=0.1)"
# The cutoff of 0.1 (10% of similarity per cluster) was selected subjectively based on the correspondence with classes


## CONSENSION: Creating a consensus sequences per cluster

./consension -i $ALIGNMENT_SANGER -t ${ALIGNMENT_SANGER/.fasta/.phylip.opti_mcc.list} -o "sanger_consensus_raw.fasta" -K 3 -R T -T T

sed -i 's/\.//g' "sanger_consensus_raw.fasta"
```

### 7 - R code to choose Sanger OTU, prepare the reference tree (reference sequences + outgroups) and prepare the names of sequences

```r
## Choosing the most interesting OTU after treatment in Linux

sanger_otu = mothur2table("sanger/sanger_filtered_aligned.phylip.opti_mcc.list")
sanger_otu

subset(sanger_otu, otu == "Otu02")

table(word(sanger_otu$name, 1, sep = fixed("_")), sanger_otu$otu)


## Checking if the two Spumellaria clusters correspond to the two species

unique(subset(sanger_blasted_1, query %in% subset(names2order_sanger, graph_name_num %in% 
                                                    subset(sanger_otu, otu == "Otu02")$name)$initial_name)$species) # OTU2 = Rhizosphaera trigonacantha

unique(subset(sanger_blasted_1, query %in% subset(names2order_sanger, graph_name_num %in% 
                                                    subset(sanger_otu, otu == "Otu03")$name)$initial_name)$species) # OTU3 = Spongosphaera streptacantha


## Filtering the five selected OTU in the concensus and adding the tag

sanger_consensus = readDNAStringSet("sanger/sanger_consensus_raw.fasta")
sanger_consensus = sanger_consensus[which(names(sanger_consensus) %in% c("Otu01", "Otu02", "Otu03", "Otu04", "Otu05"))]
sanger_consensus

sanger_consensus = c(sanger_consensus[1], sanger_consensus[3], sanger_consensus[2], sanger_consensus[4:5]) # Changing to position of OTU to be the same than 2020
sanger_consensus

names(sanger_consensus) = rep("Sanger_consensus", 5)
sanger_consensus

for(i in 1:length(sanger_consensus)){
  
  sanger_fasta = c(rbind(paste0(">", as.character(names(sanger_consensus)[i])), paste(sanger_consensus[i])))
  write.table(sanger_fasta, file = paste0("final_seqs/sanger_consensus_otu", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}
```

### 8 - Assembling all consensus from MinION and Sanger in one fasta file

```r
## Assembling all concensus in one fasta file

for(i in 1:5){
  
  sanger_consensus = readDNAStringSet(paste0("final_seqs/sanger_consensus_otu", i, ".fasta"))
  
  minion_2018_consensus = readDNAStringSet(paste0("final_seqs/minion_2018_consensus_otu", i, ".fasta"))
  
  minion_2020_consensus = readDNAStringSet(paste0("final_seqs/minion_2020_consensus_otu", i, ".fasta"))
  
  minion_2018_polished = readDNAStringSet(paste0("final_seqs/minion_2018_polished_otu", i, ".fasta"))
  
  minion_2020_polished = readDNAStringSet(paste0("final_seqs/minion_2020_polished_otu", i, ".fasta"))
  
  all_consensus = c(sanger_consensus, minion_2018_consensus, minion_2020_consensus, minion_2018_polished, minion_2020_polished)
  
  all_consensus_fasta = c(rbind(paste0(">", as.character(names(all_consensus))), paste(all_consensus)))
  write.table(all_consensus_fasta, file = paste0("final_seqs/all_consensus", i, ".fasta"), row.names = F, col.names = F, quote = F)
  
}
```

### 9 - Bash code to align all different files

```sh
cd ../third_linux

mafft --thread 2 --maxiterate 1000 --globalpair "third_linux/outgroups_reference_otu1.fasta" > "third_linux/outgroups_reference_otu1_aligned.fasta.fasta"

mafft --thread 2 --maxiterate 1000 --globalpair "third_linux/outgroups_reference_otu2.fasta" > "third_linux/outgroups_reference_otu2_aligned.fasta.fasta"

mafft --thread 2 --maxiterate 1000 --globalpair "third_linux/outgroups_reference_otu3.fasta" > "third_linux/outgroups_reference_otu3_aligned.fasta.fasta"

mafft --thread 2 --maxiterate 1000 --globalpair "third_linux/outgroups_reference_otu4.fasta" > "third_linux/outgroups_reference_otu4_aligned.fasta.fasta"

mafft --thread 2 --maxiterate 1000 --globalpair "third_linux/outgroups_reference_otu5.fasta" > "third_linux/outgroups_reference_otu5_aligned.fasta.fasta"
```

### 10 - Manual step on internet 

As the option --add in MAFFT was not working for an unknown reason ("_addfile not found"), we used the online module to add the consensus and the raw sequences to the existing reference alignment: https://mafft.cbrc.jp/alignment/server/add_sequences.html


### 11 - R code to change names and ensure they are unique (the best would have been to add GenBank accession though)

```r

## Removing the new label

for(i in 1:5){
  
  sanger_consensus = readDNAStringSet(paste0("phylogeny_final_data2/sanger_consensus/consensus_outgroups_reference_otu", i, "_realigned.fasta"))
  
  names(sanger_consensus) = gsub("New[:|]", "", names(sanger_consensus))
  
  sanger_consensus_fasta = c(rbind(paste0(">", as.character(names(sanger_consensus))), paste(sanger_consensus)))
  write.table(sanger_consensus_fasta, file = paste0("phylogeny_final_data2/sanger_consensus/consensus_outgroups_reference_otu", i, "_realigned.fasta"), 
              row.names = F, col.names = F, quote = F)
  
}

for(i in 1:5){
  
  sanger_raw_2018 = readDNAStringSet(paste0("phylogeny_final_data2/sanger_raw_2018/raw_2018_outgroups_reference_otu", i, "_realigned.fasta"))
  
  names(sanger_raw_2018) = gsub("New[:|]", "", names(sanger_raw_2018))
  
  sanger_raw_2018_fasta = c(rbind(paste0(">", as.character(names(sanger_raw_2018))), paste(sanger_raw_2018)))
  write.table(sanger_raw_2018_fasta, file = paste0("phylogeny_final_data2/sanger_raw_2018/raw_2018_outgroups_reference_otu", i, "_realigned.fasta"), 
              row.names = F, col.names = F, quote = F)
  
}

for(i in 1:5){
  
  sanger_raw_2020 = readDNAStringSet(paste0("phylogeny_final_data2/sanger_raw_2020/raw_2020_outgroups_reference_otu", i, "_realigned.fasta"))
  
  names(sanger_raw_2020) = gsub("New[:|]", "", names(sanger_raw_2020))
  
  sanger_raw_2020_fasta = c(rbind(paste0(">", as.character(names(sanger_raw_2020))), paste(sanger_raw_2020)))
  write.table(sanger_raw_2020_fasta, file = paste0("phylogeny_final_data2/sanger_raw_2020/raw_2020_outgroups_reference_otu", i, "_realigned.fasta"), 
              row.names = F, col.names = F, quote = F)
  
}



## Adding a number in front of each replicated name (for RAxML)

sanger_consensus = readDNAStringSet(paste0("phylogeny_final_data2/sanger_consensus/consensus_outgroups_reference_otu", 5, "_realigned.fasta"))

sanger_consensus_names = names(sanger_consensus)

sanger_consensus_list = lapply(split(sanger_consensus_names, sanger_consensus_names), function(x) paste0(x, "_nb", 1:length(x)))

for(j in 1:length(sanger_consensus_list)) sanger_consensus_names = 
  replace(sanger_consensus_names, which(sanger_consensus_names %in% names(sanger_consensus_list)[j]), sanger_consensus_list[[j]])

length(sanger_consensus)
length(sanger_consensus_names[!duplicated(sanger_consensus_names)])

names(sanger_consensus)
sanger_consensus_names[!duplicated(sanger_consensus_names)]


for(i in 1:5){
  
  sanger_consensus = readDNAStringSet(paste0("phylogeny_final_data2/sanger_consensus/consensus_outgroups_reference_otu", i, "_realigned.fasta"))
  
  sanger_consensus_names = names(sanger_consensus)
  
  sanger_consensus_list = lapply(split(sanger_consensus_names, sanger_consensus_names), function(x) paste0(x, "_nb", 1:length(x)))
  
  for(j in 1:length(sanger_consensus_list)) sanger_consensus_names = 
    replace(sanger_consensus_names, which(sanger_consensus_names %in% names(sanger_consensus_list)[j]), sanger_consensus_list[[j]])
  
  names(sanger_consensus) = sanger_consensus_names
  
  sanger_consensus_fasta = c(rbind(paste0(">", as.character(names(sanger_consensus))), paste(sanger_consensus)))
  write.table(sanger_consensus_fasta, file = paste0("phylogeny_final_data2/sanger_consensus/consensus_outgroups_reference_otu", i, "_realigned.fasta"), 
              row.names = F, col.names = F, quote = F)
  
}

for(i in 1:5){
  
  sanger_raw_2018 = readDNAStringSet(paste0("phylogeny_final_data2/sanger_raw_2018/raw_2018_outgroups_reference_otu", i, "_realigned.fasta"))
  
  sanger_raw_2018_names = names(sanger_raw_2018)
  
  sanger_raw_2018_list = lapply(split(sanger_raw_2018_names, sanger_raw_2018_names), function(x) paste0(x, "_nb", 1:length(x)))
  
  for(j in 1:length(sanger_raw_2018_list)) sanger_raw_2018_names = 
    replace(sanger_raw_2018_names, which(sanger_raw_2018_names %in% names(sanger_raw_2018_list)[j]), sanger_raw_2018_list[[j]])
  
  names(sanger_raw_2018) = sanger_raw_2018_names
  
  sanger_raw_2018_fasta = c(rbind(paste0(">", as.character(names(sanger_raw_2018))), paste(sanger_raw_2018)))
  write.table(sanger_raw_2018_fasta, file = paste0("phylogeny_final_data2/sanger_raw_2018/raw_2018_outgroups_reference_otu", i, "_realigned.fasta"), 
              row.names = F, col.names = F, quote = F)
  
}

for(i in 1:5){
  
  sanger_raw_2020 = readDNAStringSet(paste0("phylogeny_final_data2/sanger_raw_2020/raw_2020_outgroups_reference_otu", i, "_realigned.fasta"))
  
  sanger_raw_2020_names = names(sanger_raw_2020)
  
  sanger_raw_2020_list = lapply(split(sanger_raw_2020_names, sanger_raw_2020_names), function(x) paste0(x, "_nb", 1:length(x)))
  
  for(j in 1:length(sanger_raw_2020_list)) sanger_raw_2020_names = 
    replace(sanger_raw_2020_names, which(sanger_raw_2020_names %in% names(sanger_raw_2020_list)[j]), sanger_raw_2020_list[[j]])
  
  names(sanger_raw_2020) = sanger_raw_2020_names
  
  sanger_raw_2020_fasta = c(rbind(paste0(">", as.character(names(sanger_raw_2020))), paste(sanger_raw_2020)))
  write.table(sanger_raw_2020_fasta, file = paste0("phylogeny_final_data2/sanger_raw_2020/raw_2020_outgroups_reference_otu", i, "_realigned.fasta"), 
              row.names = F, col.names = F, quote = F)
  
}
```

### 12 - Bash code to trim realigned sequences and construct the phylogenies

```sh
############## INITIALISATION ##############

cd phylogeny

# Installing trimAl
cd trimAl/source
make
cd ../..

# Installing raxml with the right version
sudo apt install raxml
cd standard-RAxML-master
make -f Makefile.SSE3.PTHREADS.gcc
cd ..



############# SANGER + CONSENSUS #############

cd sanger_consensus


##### SANGER + CONSENSUS - OTU 1 #####

ALIGNMENT="consensus_outgroups_reference_otu1_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="consensus_outgroups_reference_otu1_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_consensus_otu1_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + CONSENSUS - OTU 2 #####

ALIGNMENT="consensus_outgroups_reference_otu2_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="consensus_outgroups_reference_otu2_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_consensus_otu2_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + CONSENSUS - OTU 3 #####

ALIGNMENT="consensus_outgroups_reference_otu3_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="consensus_outgroups_reference_otu3_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_consensus_otu3_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + CONSENSUS - OTU 4 #####

ALIGNMENT="consensus_outgroups_reference_otu4_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="consensus_outgroups_reference_otu4_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_consensus_otu4_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + CONSENSUS - OTU 5 #####

ALIGNMENT="consensus_outgroups_reference_otu5_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="consensus_outgroups_reference_otu5_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_consensus_otu5_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


############# SANGER + RAW 2018 #############

cd ../sanger_raw_2018

##### SANGER + RAW 2018 - OTU 1 #####

ALIGNMENT="raw_2018_outgroups_reference_otu1_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2018_outgroups_reference_otu1_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2018_otu1_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2018 - OTU 2 #####

ALIGNMENT="raw_2018_outgroups_reference_otu2_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2018_outgroups_reference_otu2_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2018_otu2_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2018 - OTU 3 #####

ALIGNMENT="raw_2018_outgroups_reference_otu3_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2018_outgroups_reference_otu3_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2018_otu3_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2018 - OTU 4 #####

ALIGNMENT="raw_2018_outgroups_reference_otu4_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2018_outgroups_reference_otu4_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2018_otu4_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2018 - OTU 5 #####

ALIGNMENT="raw_2018_outgroups_reference_otu5_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2018_outgroups_reference_otu5_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2018_otu5_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


############# SANGER + RAW 2020 #############

cd ../sanger_raw_2020

##### SANGER + RAW 2020 - OTU 1 #####

ALIGNMENT="raw_2020_outgroups_reference_otu1_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2020_outgroups_reference_otu1_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2020_otu1_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2020 - OTU 2 #####

ALIGNMENT="raw_2020_outgroups_reference_otu2_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2020_outgroups_reference_otu2_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2020_otu2_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2020 - OTU 3 #####

ALIGNMENT="raw_2020_outgroups_reference_otu3_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2020_outgroups_reference_otu3_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2020_otu3_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2020 - OTU 4 #####

ALIGNMENT="raw_2020_outgroups_reference_otu4_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2020_outgroups_reference_otu4_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2020_otu4_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED


##### SANGER + RAW 2020 - OTU 5 #####

ALIGNMENT="raw_2020_outgroups_reference_otu5_realigned.fasta"
TRIM_THRESHOLD="30"

# Trimming alignment
TRIMMED="raw_2020_outgroups_reference_otu5_trimmed.fasta"
./../trimAl/source/trimal -in $ALIGNMENT -out $TRIMMED -gt 0.$TRIM_THRESHOLD

# Phylogenetic inference (GTR+CAT)
BS="1000"
THREADS="1"
OUTPUT="cat_sanger_raw_2020_otu5_BS"
raxmlHPC-PTHREADS-SSE3 -T $THREADS -m GTRCAT -c 25 -p $RANDOM -x $(date +%s) -d -f a -N $BS -n $OUTPUT -s $TRIMMED
```

### 13 - R code to construct the different barplots and compute the error rate per OTU commited by MinION

```r
## Preparing the file for barplotting

sanger_blasted_1_graph = tibble(cbind(sanger_blasted_1, tibble(guppy = "sanger", Qscore = NA)))
sanger_blasted_1_graph

sanger_length = tibble(query = names(sanger_filtered), Length = width(sanger_filtered))
sanger_length

sanger_blasted_1_graph = tibble(merge(sanger_blasted_1_graph, sanger_length))

all_best_match_graph = rbind(minion_blast_guppy, sanger_blasted_1_graph)

all_best_match_graph$guppy = factor(all_best_match_graph$guppy, levels = c("sanger", "2018", "2020"))

all_best_match_graph = subset(all_best_match_graph, graph_name != "Dino")

all_best_match_graph = all_best_match_graph %>% group_by(guppy, graph_name) %>% 
  mutate(sd_Qscore = sd(Qscore), sd_Length = sd(Length), sd_id = sd(id))

all_best_match_graph


## Plotting the number of reads

ggplot(all_best_match_graph, aes(x = graph_name)) + 
  facet_wrap(~guppy, nrow = 3, labeller = as_labeller(c("sanger" = paste0("Sanger (total = ", nrow(sanger_blasted_1), ")"),
                                                        "2018" = paste0("MinION: GUPPY 2018 (total = ", nrow(minion_2018_blasted_1), ")"),
                                                        "2020" = paste0("MinION: GUPPY 2020 (total = ", nrow(minion_2020_blasted_1), ")")))) + 
  geom_histogram(stat = "count") +
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Number of reads")
ggsave("Reads number.png", dpi = 900)


## Plotting the Qscore for MinION

ggplot(subset(all_best_match_graph, guppy != "sanger"), aes(x = graph_name, y = as.numeric(Qscore))) + 
  facet_wrap(~guppy, nrow = 2, labeller = as_labeller(c("2018" = paste0("MinION: GUPPY 2018 (overall mean = ", round(median(subset(minion_blast_guppy, guppy == 2018)$Qscore), 2), 
                                                                        " ± ", round(sd(subset(minion_blast_guppy, guppy == 2018)$Qscore), 2), ")"),
                                                        "2020" = paste0("MinION: GUPPY 2020 (overall mean = ", round(median(subset(minion_blast_guppy, guppy == 2020)$Qscore), 2), 
                                                                        " ± ", round(sd(subset(minion_blast_guppy, guppy == 2020)$Qscore), 2), ")")))) +
  geom_errorbar(aes(ymin = Qscore - sd_Qscore, ymax = Qscore + sd_Qscore), width = 0, position=position_dodge(0.9)) +
  geom_bar(position = position_dodge(0.9), stat = "identity") + 
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Mean Nanopore quality score")
ggsave("MinION Qscore.png", dpi = 900)


## Plotting the length of reads

ggplot(all_best_match_graph, aes(x = graph_name, y = Length)) + 
  facet_wrap(~guppy, nrow = 3, labeller = as_labeller(c("2018" = paste0("MinION: GUPPY 2018 (overall mean = ", round(mean(subset(all_best_match_graph, guppy == "2018")$Length), 2), 
                                                                        " ± ", round(sd(subset(all_best_match_graph, guppy == "2018")$Length), 2), " bp)"),
                                                        "2020" = paste0("MinION: GUPPY 2020 (overall mean = ", round(mean(subset(all_best_match_graph, guppy == "2020")$Length), 2), 
                                                                        " ± ", round(sd(subset(all_best_match_graph, guppy == "2020")$Length), 2), " bp)"),
                                                        "sanger" = paste0("Sanger (overall mean = ", round(mean(subset(all_best_match_graph, guppy == "sanger")$Length), 2), 
                                                                        " ± ", round(sd(subset(all_best_match_graph, guppy == "sanger")$Length), 2), " bp)")))) +
  geom_errorbar(aes(ymin = Length - sd_Length, ymax = Length + sd_Length), width = 0, position=position_dodge(0.9)) +
  geom_bar(position=position_dodge(0.9), stat = "identity") + 
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Mean read length (bp)")
ggsave("Reads length.png", dpi = 900)


## Plotting the percentage of identity

ggplot(all_best_match_graph, aes(x = graph_name, y = id)) + 
  facet_wrap(~guppy, nrow = 3, labeller = as_labeller(c("2018" = paste0("MinION: GUPPY 2018 (overall mean = ", round(mean(subset(all_best_match_graph, guppy == "2018")$id), 2), 
                                                                        " ± ", round(sd(subset(all_best_match_graph, guppy == "2018")$id), 2), "%)"),
                                                        "2020" = paste0("MinION: GUPPY 2020 (overall mean = ", round(mean(subset(all_best_match_graph, guppy == "2020")$id), 2), 
                                                                        " ± ", round(sd(subset(all_best_match_graph, guppy == "2020")$id), 2), "%)"),
                                                        "sanger" = paste0("Sanger (overall mean = ", round(mean(subset(all_best_match_graph, guppy == "sanger")$id), 2), 
                                                                          " ± ", round(sd(subset(all_best_match_graph, guppy == "sanger")$id), 2), "%)")))) +
  geom_errorbar(aes(ymin = id - sd_id, ymax = id + sd_id), width = 0, position=position_dodge(0.9)) +
  geom_bar(position = "dodge", stat = "identity") + 
  scale_y_continuous(labels = function(x) paste0(x, "%")) +
  theme_() + theme(axis.title = element_text(size = 14), strip.text = element_text(size = 14),
                   axis.text.x = element_text(size = 12, angle = 90, hjust = 1, vjust = 0.2),
                   axis.text.y = element_text(size = 11)) +
  labs(x = "Class", y = "Mean similarity with best match")
ggsave("Best match id.png", dpi = 900)


## Plotting the percentage of identity

otu_name = c("Nassellaria", "Spumellaria 1", "Spumellaria 2", "Chrysophyceae", "Bacillariophyta")

for(i in 1:5){
  
  all_consensus_otu = readDNAStringSet(paste0("Trees/sanger_consensus/consensus_outgroups_reference_otu", i, "_trimmed.fasta"))
  all_consensus_otu = all_consensus_otu[grep("consensus", names(all_consensus_otu))]
  names(all_consensus_otu) = word(word(names(all_consensus_otu), 1, sep = fixed(" ")), 1, sep = fixed("_n"))
  
  all_consensus_otu_distance = round(adist(all_consensus_otu) / width(all_consensus_otu) * 100, 2)
  consensus_otu_distance_sanger = all_consensus_otu_distance[which(rownames(all_consensus_otu_distance) == "Sanger_consensus"), ]
  consensus_otu_distance_sanger = t(as.data.frame(consensus_otu_distance_sanger))
  consensus_otu_distance_sanger = consensus_otu_distance_sanger[, -which(colnames(consensus_otu_distance_sanger) == "Sanger_consensus")]
  consensus_otu_distance_sanger = tibble(otu = otu_name[i], consensus = names(consensus_otu_distance_sanger), 
                                         similarity_sanger = consensus_otu_distance_sanger)
  
  if(i == 1) all_otu_consensus_distance_sanger = consensus_otu_distance_sanger
  
  else all_otu_consensus_distance_sanger = rbind(all_otu_consensus_distance_sanger, consensus_otu_distance_sanger)
  
}

all_otu_consensus_distance_sanger

all_otu_consensus_distance_sanger$otu = factor(all_otu_consensus_distance_sanger$otu, levels = otu_name)

ggplot(all_otu_consensus_distance_sanger, aes(x = otu, y = similarity_sanger, fill = otu)) +
  facet_wrap(~ consensus, ncol = 2, nrow = 2, labeller = as_labeller(c("Only_consensus_2018" = "GUPPY 2018 - Raw consensus",
                                                                       "Only_consensus_2020" = "GUPPY 2020 - Raw consensus",
                                                                       "Polished_consensus_2018" = "GUPPY 2018 - Polished consensus",
                                                                       "Polished_consensus_2020" = "GUPPY 2020 - Polished consensus"))) +
  geom_bar(position = "dodge", stat = "identity") +
  geom_text(aes(label = paste0(similarity_sanger, "%")), vjust = -1, family = "Segoe UI Semilight") + 
  scale_y_continuous(limits = c(0, 33.5), labels = function(x) paste0(x, "%")) +
  guides(fill = guide_legend(title = "OTU")) + ylab("Distance with Sanger consensus") +
  theme_() + theme(axis.text.x = element_blank(), axis.title.x = element_blank(), axis.ticks.x = element_blank(),
                   axis.text.y = element_text(size = 12), axis.title.y = element_text(size = 14),
                   legend.text = element_text(size = 12), legend.title = element_text(size = 14),
                   strip.text = element_text(size = 12))
ggsave("Error rate MinION.png", dpi = 900)
```
