## Récupération des séquences fasta des outgroups

A partir des espèces outgroup sélectionnées dans la littérature disponible [ici](https://gitlab.com/eliot_ruiz/minion/-/blob/main/Day%203%20(01/12/2021)/notebook_day3.md), nous avons recherché sur la banque de données [NCBI](https://www.ncbi.nlm.nih.gov/) les séquences de l'ADN ribosomal. Comme la plupart des organismes sont peu renseignés, nous avons donc sélectionné les séquences de l'ADNr 18S directement. 
Nous avons alors récupéré les séquences ADNr18S au format FASTA sur **chaque** individu, puis les avons regroupé. Ces séquences FASTA sont disponibles [ici](https://gitlab.com/eliot_ruiz/minion/-/tree/main/Outgroups). 

Concernant les 2 grands groupes (Spumellaria & Nassellaria), la pré-publication de Miguel Sandin (https://doi.org/10.1101/2021.10.05.463214) utilise déjà des groupes d'espèces en outgroup. Nous avons utlisé les mêmes : ils nous a suffit de récupérer la liste des espèces utlisées dans le matériel supplémentaire disponible [sur son GitHub directement](https://github.com/MiguelMSandin/IntraGenomic-variability). *

## Réalisation d'un schéma manuscrit de la pipeline

Durant 30mn, nous avons tous trois tenté de faire un schéma manuscrit et exhaustif de la pipeline bioinformatique nous avons élaboré. Cela nous as permis de nous assurer de bien consigner toutes les étapes effectuées afin de ne pas en oublier, ainsi que mettre au clair les points que nous n'avions pas tous compris. Suite à des essais préliminaires sur le tableau de la salle, nous avons mis au propre nos schéma sur une feuille, ce qui nous as alors simplifié la tâche pour la réalisation du diaporama.

## Réalisation de la présentation 

Nous avons rédiger le diaporama qui nous servira de support lors de la présentation orale du 03/12/2021. Le diaporama est [disponible ici en ligne](https://docs.google.com/presentation/d/1zwa6n6gRIzevW_BzmaTpcrJ-J5atydBtBOW9IJePeyA/edit?fbclid=IwAR06bqOdyV-nDqbBtXlDzSwFOr-GmBllWjt-hSPu5eqM5iNmmEOjbfY3SPM#slide=id.g105c478331f_0_431).


## Presentation préliminaire des figures en groupes

Nous avons présenté devant les classes les figures préliminaires obtenues à l'issu du traitement informatique des séquences. 
Ces premières figures sont [disponibles ici]() et représentent surtout des informations sur la qualité des reads et le nombre, ainsi qu'un premier arbre phylogénétique réalisé avec les séquences consensus issues des fichiers fastq de Guppy 2020. 


## Réunion avec Miguel

Discussion avec Miguel concernant l'avancée du projet - finalisation du protocoles de bio informatique souhaiter et décision des arbres phylogénétiques que nous allons construire.

## Pipeline Bio-informatique

Après les complications d'utilisation de Windows pour l'utilisation de certains programmes ne fonctionnant que sous Linux, nous avons décidé d'utiliser les ordinateurs de la salle informatique. Etant que ces ordinateurs ne possédaient pas de connection internet, une partie des scripts ont été écrits en bash pour tourner sur les ordinateurs de la salle Linux, tandis que le reste des opérations étaient effectuées sur nos ordinateurs. Cela a requis de très nombreux allers-retours avec la clé USB ce qui n'était pas le plus optimal mais nous n'avions pas le choix... 

Cette journée de travail intense nous as permis de très largement avancer sur le code, puisque nous savions maintenant quelles techniques utiliser et que nous avions formalisé de manière claire les étapes que nous allions effectuer. Nous avons donc pu réaliser l'ensemble du code depuis le clustering jusqu'au lancement de la construction des arbres phylogénétiques, qui ont ainsi pu tourner pendant la nuit. Le temps de calcul étant d'environ 3h par arbres, nous avons utilisé 5 ordinateurs de la salle simultanément pour faire tourner les calculs qui ont tout de même fini peu de temps avant le début de l'oral pour certains.

